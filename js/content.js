// For progressiveMedia Plugin (Medium website).
$('.progressiveMedia').each((index, element) => {
  let image = $(element).find('.progressiveMedia-image');
  image.attr('src', image.data('src'));
  $(element).addClass('is-imageLoaded');
});

$('img').each((index, element) => {
  $(element).attr('src', $(element).data('src'));
});

// GearBest support.
$('.js-descLazyload').each((index, element) => {
  $(element).attr('src', $(element).data('lazy'));
});

// Jetpack Lazy Images.
$('.jetpack-lazy-image, [data-lazy-src]').each((index, element) => {
  $(element).attr('src', $(element).data('lazySrc'));
});

// banggood.
$('img.bg_lazy').each((index, element) => {
  $(element).attr('src', $(element).data('original'));
});
